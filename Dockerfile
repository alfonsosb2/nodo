FROM maven:3.5-ibmjava-8

ADD target/nodo*.jar /nodo.jar

ADD start.sh /

ENTRYPOINT ["/start.sh"]