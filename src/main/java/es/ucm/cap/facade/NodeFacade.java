package es.ucm.cap.facade;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class NodeFacade {

  private static final Log LOGGER = LogFactory.getLog(NodeFacade.class);

  public ResponseEntity<?> calculate(int num) {
    Long number = fibonacci(Long.valueOf(num));
    LOGGER.info("Entrada: " + num + " --> Salida: " + number);
    return new ResponseEntity<Long>(number, HttpStatus.OK);
  }

  private Long fibonacci(Long n) {
    if (n > 1) {
      return fibonacci(n - 1) + fibonacci(n - 2); // función recursiva
    } else if (n == 1) { // caso base
      return 1L;
    }
    return 0L;
  }
}
